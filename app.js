var app = angular.module("mysite",["ngRoute"]);
app.config(function($routeProvider){
		$routeProvider
			.when("/",{
				templateUrl:"/",			
				controller:"homeCtrl"
			}).when("/about",{
				templateUrl:"/",			
				controller:"aboutCtrl"
			}).when("/blog",{
				template:"<h2> Coming soon !</h2>",			
				controller:"homeCtrl"
			});
			
	});
	
	
Job Location:  Pune Only (Pune-based candidates will be preferred)

Experience(In Years) : 2 - 5 years of relevant experience

Job Functions / Description:

 

    Work closely with Data Scientists, Project Managers to deliver scalable RESTful API’s in Node & front end using Angular JS v4 or 5 recommended
    Design underlying data models for applications and services to be scalable and maintainable (Relational and Document Based Databases)
    Identify improvements and participate on innovating tools and services
    Actively practice TDD & BDD while developing new features and refactoring existing code
    Provide operational support by fixing production issues for API's and backend services 

 

Qualifications:

 

BE degree in Computer Science or related field

 

Skills Set : MVC Development with Front End Experience

 

    Proficiency in Computer Science fundamentals, including object-oriented design, data structures, algorithms, problem solving, and complexity analysis
    Proficiency in at least one language: Python (Recommended)/PHP
    Middleware Framework considered: Django/Flask/ Web2Py/ TurboGears and Nodejs
    Decent knowledge of Front End technologies : HTML, CSS, Bootstrap, Ajax, JavaScript, Angular JS v 4/5 and Reactjs
    Familiarity with web and user-interface development : JavaScript, HTML5, CSS, LESS or SASS are a plus
    Should have good experience with AWS / GCP
    Create and maintain RESTful API’s